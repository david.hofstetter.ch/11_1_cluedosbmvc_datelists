package ch.bbw.pr.cluedo.model;

/**
 * A Person
 * @author Peter Rutschmann
 * @version 29.08.2022
 */
public class Person {
   private String name;
   private String formOfAdress;

   public String getJob() {
      return job;
   }

   public void setJob(String job) {
      this.job = job;
   }

   private String job;

   public Person(String name, String formOfAdress, String job) {
      this.name = name;
      this.formOfAdress = formOfAdress;
      this.job = job;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getFormOfAdress() {
      return formOfAdress;
   }

   public void setFormOfAdress(String formOfAdress) {
      this.formOfAdress = formOfAdress;
   }



   @Override
   public String toString() {
      return "Person{" +
            "name='" + name + '\'' +
            ", formOfAdress='" + formOfAdress + '\'' +
              "job=" + job + '\'' +
            '}';
   }
}
